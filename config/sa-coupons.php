<?php

return [
    'api' => [
        'host' => env('SACOUPONS_API_HOST', 'https://staging.sacoupons.co.za/api/v1/'),
        'key' => env('SACOUPONS_API_KEY')
    ]
];
