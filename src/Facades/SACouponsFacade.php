<?php

namespace SayThanks\SACoupons\Facades;

use Illuminate\Support\Facades\Facade;

class SACouponsFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'sa-coupons';
    }
}
