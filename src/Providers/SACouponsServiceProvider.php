<?php

namespace SayThanks\SACoupons\Providers;

use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\ServiceProvider;
use SayThanks\SACoupons\SACouponsClient;

class SACouponsServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../../config/sa-coupons.php',
            'sa-coupons'
        );

        $this->app->bind(
            'sa-coupons',
            static function ($app, $params) {
                config('sa-coupons.api.key');
                $apiKey = $params['key'] ?? config('sa-coupons.api.key');

                if ($apiKey === null) {
                    throw new BindingResolutionException(
                        'Cannot create SACoupons Client: API key is missing. 
                        Please ensure SACOUPONS_API_APIKEY is in your .env or provide a username when creating the client.'
                    );
                }

                return new SACouponsClient($apiKey);
            }
        );
    }
}
