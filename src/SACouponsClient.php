<?php

namespace SayThanks\SACoupons;

use Carbon\Carbon;
use Illuminate\Http\Client\PendingRequest;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class SACouponsClient
{
    public PendingRequest $client;

    private string $apiKey;

    public function __construct(string $apiKey)
    {
        $this->client = Http::retry(3, 100)
            ->baseUrl(config('sa-coupons.api.host'))
            ->asJson()
            ->acceptJson();
        $this->apiKey = $apiKey;
    }

    public function getVoucherCategories(int $from = 0, int $limit = 10, ?string $msisdn = null): array
    {
        $data = ['from' => $from, 'limit' => $limit];
        if (!empty($msisdn)) {
            $data['msisdn'] = $msisdn;
        }
        return $this->post('get_voucher_categories', $data)->json();
    }

    public function getVoucherList(int $from = 0, int $limit = 10, ?string $categoryId = null, ?string $msisdn = null): array
    {
        $data = ['from' => $from, 'limit' => $limit];
        if (!empty($categoryId)) {
            $data['category_id'] = $categoryId;
        }
        if (!empty($msisdn)) {
            $data['msisdn'] = $msisdn;
        }
        return $this->post('get_voucher_list', $data)->json();
    }

    public function getVoucher(string $voucherId = null, ?string $msisdn = null): array
    {
        $data = ['voucher_id' => $voucherId];
        if (!empty($msisdn)) {
            $data['msisdn'] = $msisdn;
        }
        return $this->post('get_voucher', $data)->json();
    }

    public function getVoucherCode(string $voucherId, string $msisdn, ?string $clientRequestId = null, bool $generateBarcodeImage = false): array
    {
        $data = [
            'voucher_id' => $voucherId,
            'msisdn' => $msisdn,
            'client_request_id' => $clientRequestId,
            'generate_barcode_img' => $generateBarcodeImage
        ];
        if (!empty($clientRequestId)) {
            $data['client_request_id'] = $clientRequestId;
        }
        return $this->post('get_voucher_code', $data)->json();
    }

    public function sendVoucherCode(string $voucherId, string $msisdn, ?string $clientRequestId, bool $sendCode = true, bool $generateBarcodeImage = false): array
    {
        $data = [
            'voucher_id' => $voucherId,
            'msisdn' => $msisdn,
            'send_code' => $sendCode,
            'client_request_id' => $clientRequestId,
            'generate_barcode_img' => $generateBarcodeImage
        ];
        if (!empty($clientRequestId)) {
            $data['client_request_id'] = $clientRequestId;
        }
        return $this->post('send_voucher_code', $data)->json();
    }

    public function resendVoucherCode(string $voucherId, string $msisdn, ?string $voucherCode = null, ?string $voucherCodeHash = null, ?string $clientRequestId = null): array
    {
        $data = [
            'voucher_id' => $voucherId,
            'msisdn' => $msisdn,
        ];
        if (!empty($voucherCode)) {
            $data['voucher_code'] = $voucherCode;
        }
        if (!empty($voucherCodeHash)) {
            $data['voucher_code_hash'] = $voucherCodeHash;
        }
        if (!empty($clientRequestId)) {
            $data['client_request_id'] = $clientRequestId;
        }
        return $this->post('resend_voucher', $data)->json();
    }

    public function getVoucherCodeRedemptionStatus(string $voucherId, ?string $voucherCode = null, ?string $voucherCodeHash = null, ?string $clientRequestId = null): array
    {
        $data = [
            'voucher_id' => $voucherId,
        ];
        if (!empty($voucherCode)) {
            $data['voucher_code'] = $voucherCode;
        }
        if (!empty($voucherCodeHash)) {
            $data['voucher_code_hash'] = $voucherCodeHash;
        }
        if (!empty($clientRequestId)) {
            $data['client_request_id'] = $clientRequestId;
        }
        return $this->post('get_voucher_code_status', $data)->json();
    }

    public function updateVoucherCodeRedemptionStatus(string $voucherId, Carbon $redeemedAt, ?string $voucherCodeHash, ?string $clientRequestId = null, ?string $storeName = null, ?string $storeNote = null): array
    {
        $data = [
            'voucher_id' => $voucherId,
            'redeemed_at' => $redeemedAt->toDateTimeString(),
        ];
        if (!empty($voucherCodeHash)) {
            $data['voucher_code_hash'] = $voucherCodeHash;
        }
        if (!empty($clientRequestId)) {
            $data['client_request_id'] = $clientRequestId;
        }
        if (!empty($storeName)) {
            $data['store_name'] = $storeName;
        }
        if (!empty($storeNote)) {
            $data['store_note'] = $storeNote;
        }
        return $this->post('update_voucher_code_redemption_status', $data)->json();
    }

    public function getVoucherRedemptions(string $voucherId, Carbon $startDate, Carbon $endDate): array
    {
        $data = [
            'voucher_id' => $voucherId,
            'start_date' => $startDate->toDateTimeString(),
            'end_date' => $endDate->toDateTimeString()
        ];
        return $this->post('get_voucher_redemptions', $data)->json();
    }
    
    private function get(string $url, array $queryData = []): \GuzzleHttp\Promise\PromiseInterface|Response
    {
        return $this->request('get', $url, $queryData);
    }

    private function post(string $url, array $data = []): \GuzzleHttp\Promise\PromiseInterface|Response
    {
        $data = array_merge(['api_key' => $this->apiKey], $data);
        return $this->request('post', $url, $data);
    }

    private function request(string $method, string $url, array $data = []): \GuzzleHttp\Promise\PromiseInterface|Response
    {
        if ($method !== 'get' && $method !== 'post') {
            throw new \InvalidArgumentException("SACouponsClient: unsupported request type $method specified - expected 'get' or 'post'.");
        }

        Log::info("SACouponsClient: Sending $method request", ['url' => $url, 'query' => $data]);
        /** @var \GuzzleHttp\Promise\PromiseInterface|Response $response */
        $response = $this->client->$method($url, $data);

        $logData = [
            'url' => $url,
            'data' => $data,
            'response_code' => $response->status(),
            'response_json' => $response->json(),
        ];
        if ($response->successful()) {
            Log::debug("SACouponsClient: $method request successful", $logData);
        } else {
            Log::warning("SACouponsClient: $method request unsuccessful", $logData);
        }
        return $response;
    }
}
